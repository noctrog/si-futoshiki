#include "solver.h"
#include <qnamespace.h>
#include <numeric>
#include <algorithm>
#include <chrono>
#include <cmath>

Cronometro::Cronometro(const std::string _nombre_algoritmo)
    : nombre_algoritmo(_nombre_algoritmo),
    comienzo(std::chrono::steady_clock::now())
{

}

Cronometro::~Cronometro()
{
    auto fin = std::chrono::steady_clock::now();
    auto duracion = std::chrono::duration<float>(fin - comienzo);
    std::cout << nombre_algoritmo << " ha tardado "
        << duracion.count() << "s" << std::endl;
}

Dominio::Dominio(const int _size)
    : size(_size), dominio(_size * _size, std::set<int>())
{
    // Inicializa la matriz tridimensional que representa el dominio
    for (int i = 0; i < _size * _size; ++i) {
        for (int k = 1; k <= _size; ++k)
            dominio.at(i).insert(k);
    }
}

Dominio::Dominio(Tablero *const tablero)
    : size(tablero->getSize()), dominio(size * size, std::set<int>())
{
    // Inicializa la matriz tridimensional que representa el dominio
    for (int i = 0; i < size; ++i) {
        for (int j = 0; j < size; ++j) {
            // Comprobar que no hay una restriccion inicial en la casilla
            if (tablero->getCasilla(i, j) == 0)
                for (int k = 1; k <= size; ++k)
                    getDominio(i, j).insert(k);
            else
                getDominio(i, j) = {tablero->getCasilla(i, j)};
        }
    }
}

Dominio::Dominio(const Dominio& rhs)
    : size(rhs.size), dominio(rhs.dominio)
{

}

Dominio::~Dominio()
{

}

std::set<int>& Dominio::getDominio(const int i, const int j)
{
    return dominio.at(i*size + j);
}

const std::set<int>& Dominio::getDominio(const int i, const int j) const
{
    return dominio.at(i*size + j);
}

std::set<int>& Dominio::getDominio(const int i)
{
    return dominio.at(i);
}

const std::set<int>& Dominio::getDominio(const int i) const
{
    return dominio.at(i);
}

int Dominio::getNumValores() const
{
    int num_valores_total = 0.0f;
    for (auto dom : dominio)
        num_valores_total += dom.size();

    return num_valores_total;
}

ArcosCSP::ArcosCSP(Tablero* const tablero)
{
    if (!tablero)
        return;

    // Consigue el indice de la variable
    auto variable = [tablero](const int i, const int j){return i*tablero->getSize() + j;};

    int l = 0;
    for (int i = 0; i < tablero->getSize(); ++i) {
        for (int j = 0; j < tablero->getSize(); ++j) {
            // Insertar las restricciones de valores diferentes para toda su columna y fila
            for (int k = 0; k < tablero->getSize(); ++k) {
                // Restricciones de diferencia por columnas
                if (j != k) {
                    l++;
                    insertarRestriccion(variable(i, j), variable(i, k), Restriccion::DIFERENTE);
                }
                if (i != k) {
                    l++;
                    insertarRestriccion(variable(i, j), variable(k, j), Restriccion::DIFERENTE);
                }
            }

            // Insertar restricciones de mayor o menor que, en caso de que existan
            if (i < tablero->getSize()-1 && tablero->getElement(i*2+1, j*2)) {
                if (tablero->getElement(i*2+1, j*2) == 1) {
                    insertarRestriccion(variable(i,   j), variable(i+1, j), Restriccion::MAYOR_QUE);
                    insertarRestriccion(variable(i+1, j), variable(i,   j), Restriccion::MENOR_QUE);
                } else {
                    insertarRestriccion(variable(i,   j), variable(i+1, j), Restriccion::MENOR_QUE);
                    insertarRestriccion(variable(i+1, j), variable(i,   j), Restriccion::MAYOR_QUE);
                }
            }
            if (j < tablero->getSize()-1 && tablero->getElement(i*2, j*2+1)) {
                if (tablero->getElement(i*2, j*2+1) == 1) {
                    insertarRestriccion(variable(i, j), variable(i, j+1), Restriccion::MAYOR_QUE);
                    insertarRestriccion(variable(i, j+1), variable(i, j), Restriccion::MENOR_QUE);
                } else {
                    insertarRestriccion(variable(i, j), variable(i, j+1), Restriccion::MENOR_QUE);
                    insertarRestriccion(variable(i, j+1), variable(i, j), Restriccion::MAYOR_QUE);
                }
            }
        }
    }
}

ArcosCSP::~ArcosCSP()
{

}

void ArcosCSP::insertarRestriccion(const int var1, const int var2, const Restriccion r)
{
    // Si la entrada al diccionario no existe, crearla
    const auto it = arcos.find(var2);
    if (it == arcos.end()) {
        arcos.insert({var2, {{var1, r}}});
    } else {
        // Si la entrada ya existe, simplemente insertar
        it->second.push_back({var1, r});
    }
}

std::vector<arco> ArcosCSP::getRestriccionesVecinas(const int var) const
{
    // Conseguir las restricciones vecinas
    auto restricciones_it = arcos.find(var);

    // En caso de no encontrar nada, devolver un vector vacio
    if (restricciones_it == arcos.end())
        return {};

    // Crear el vector con todos los arcos vecinos
    auto& restricciones = restricciones_it->second;
    std::vector<arco> arcos_vecinos;
    for (size_t i = 0; i < restricciones.size(); ++i) {
        arcos_vecinos.push_back({std::get<0>(restricciones[i]), var, std::get<1>(restricciones[i])});
    }

    return arcos_vecinos;
}

cola_ac3 ArcosCSP::crearCola(void) const
{
    cola_ac3 cola;

    for (arcos_iniciales::const_iterator it = arcos.begin(); it != arcos.end(); ++it) {
        for (auto vec_i : it->second) {
            cola.insert({std::get<0>(vec_i), it->first, std::get<1>(vec_i)});
        }
    }

    return cola;
}

Solver::Solver(QObject *parent) 
    : QObject(parent), backTrackingJumps(0), fcBackJumps(0)
{

}

void Solver::ejecutarBT(Tablero *tablero)
{
    backTrackingJumps = 0;

    // Guarda el tablero que se va a resolver
    this->tablero = tablero;

    // Inicializa un dominio con todas las posibles opciones para cada casilla,
    // en el caso de que no exista uno ya creado por AC3
    if (!dominio_ptr) 
        dominio_ptr = std::unique_ptr<Dominio>(new Dominio(tablero));

    // El cronometro empieza a contar
    Cronometro crono("Backtracking");

    // Llama a la funcion recursiva desde la posición (0, 0)
    backtracking(0, 0, *dominio_ptr);

    // Imprime el número de saltos hacia atras
    std::cout << "Saltos hacia atras: " << backTrackingJumps << std::endl;
}

bool Solver::backtracking(int i, int j, const Dominio& dominio)
{
    // Explorar la siguiente casilla
    if (j == tablero->getSize()) j = 0, ++i;

    // Mirar si se ha terminado de asignar todas las variables
    if (i == tablero->getSize()) return true;

    // Prueba todos los valores del dominio en la casilla seleccionada
    for (const int n : dominio.getDominio(i, j)) {
        // Comprueba si el valor seleccionado es factible
        tablero->setCasilla(i, j, n);
        if (btComprobarCasilla(i, j)) {
            // Probar recursivamente las siguientes casillas
            bool solucion = backtracking(i, j+1, dominio);
            // Si se ha encontrado la solucion, finaliza
            if (solucion) return true;
        }
    }

    // No se ha encontrado ninguna solucion y se hace un salto hacia atras
    tablero->setCasilla(i, j, 0);
    backTrackingJumps++;
    return false;
}

// Devuelve true si la casilla cumple con las restricciones del tablero
bool Solver::btComprobarCasilla(const int i, const int j) const
{
    if (i > 0 && tablero->comprobarBinariaSiguienteVertical(i-1, j))
        return false;
    if (j > 0 && tablero->comprobarBinariaSiguienteHorizontal(i, j-1))
        return false;
    if (tablero->estaEnFila(i, j) || tablero->estaEnCol(i, j))
        return false;

    return true;
}

void Solver::ejecutarAC3(Tablero *tablero)
{
    // Guardar el tablero en la propia clase
    this->tablero = tablero;

    // Inicializar el dominio con todos los posibles valores para cada variable
    dominio_ptr = std::unique_ptr<Dominio>(new Dominio(tablero));

    // Comenzar a cronometrar
    Cronometro crono("AC3");

    // Ejecutar el algoritmo AC3 para reducir los dominios de las variables
    AC3(*dominio_ptr);

    // Muestra el porcentaje de valores eliminados del dominio
    std::cout << "Numero de valores al inicio: " << std::pow(tablero->getSize(), 3) << std::endl;
    std::cout << "Numero de valores al final: " << dominio_ptr->getNumValores() << std::endl;

    return;
}

bool Solver::AC3(Dominio& dominio)
{
    // Conseguir las restricciones binarias iniciales
    ArcosCSP rinic(tablero);

    // Crear la cola y llenarla con todos los pares de valores entre los cuales existen restricciones
    cola_ac3 cola = rinic.crearCola();

    while (!cola.empty()) {
        // Seleccionar y borrar
        int Vk, Vm; Restriccion r;
        std::tie(Vk, Vm, r) = *cola.begin();
        cola.erase(cola.begin());

        bool cambio = false;
        auto& Dk = dominio.getDominio(Vk);
        for (auto it = Dk.begin(); it != Dk.end();) {
            if (noConsistente(*it, dominio.getDominio(Vm), r)) {
                // Borrar si no es consistente
                it = Dk.erase(it);
                cambio = true;
            } else {
                ++it;
            }
        }

        // Retorna sin solucion
        if (Dk.empty()) {
            return false;
        }

        if (cambio == true) {
            // Anadir arcos vecinos
            for (auto& arco : rinic.getRestriccionesVecinas(Vk)) {
                cola.insert(arco);
            }
        }
    }

    return true;
}

bool Solver::noConsistente(const int vk, const std::set<int>& dom, const Restriccion r) const
{
    for (const auto vm : dom) {
        switch (r) {
            case Restriccion::DIFERENTE:
                if (vk != vm) return false;
                break;
            case Restriccion::MAYOR_QUE:
                if (vk < vm) return false;
                break;
            case Restriccion::MENOR_QUE:
                if (vk > vm) return false;
                break;
            default:
                break;
        }
    }

    return true;
}

void Solver::ejecutarFC(Tablero *tablero)
{
    fcBackJumps = 0;

    size_t N = tablero->getSize() * tablero->getSize();

    // Guardar tablero
    this->tablero = tablero;

    // Inicializa un dominio con todas las posibles opciones para cada casilla,
    // en el caso de que no exista uno ya creado por AC3
    if (!dominio_ptr) 
        dominio_ptr = std::unique_ptr<Dominio>(new Dominio(tablero));

    // Contenedor de soluciones
    FC_X X(N, 0);

    // Vector de valores podados
    Podados podados(N);

    // Inicializar el tablero de restricciones
    iniTableroRestriccion();

    // Comenzar a cronometrar
    Cronometro crono("Forward Checking");

    // Forward Checking
    FC(0, X, *dominio_ptr, podados);

    // Pasar soluciones al tablero
    guardarEnTablero(X);

    // Mostrar por salida estandar el numero de saltos atras realizados
    std::cout << "Numero de saltos hacia atras: " << fcBackJumps << std::endl;

    return;
}

bool Solver::FC(const int i, FC_X& X, Dominio& factibles, Podados& podados)
{
    // Para cada de los valores posibles
    for (int a : factibles.getDominio(i)) {
        // Guardar instancia actual 
        X[i] = a;
        // Si se ha completado el tablero, retornar
        if (static_cast<size_t>(i) == X.size() - 1)
            return true;
        else {
            if (forward(i, a, factibles, podados))
                if (FC(i+1, X, factibles, podados)) 
                    return true;
            restaura(i, factibles, podados);
        }
    }

    return false;
}

bool Solver::forward(const int i, const int a, Dominio& factibles, Podados& podados)
{
    int N = podados.size();
    for (int j = i+1; j < N; ++j) {
        bool vacio = true;
        for(auto it = factibles.getDominio(j).begin(); it != factibles.getDominio(j).end();) {
            int b = *it;
            if (cumpleRestriccion(a, b, i, j)) {
                vacio = false;
                ++it;
            }
            else {
                // Eliminar b de factible[j]
                it = factibles.getDominio(j).erase(it);
                // Anadir b a podado[j]
                podados[j].insert(std::make_pair(i, b));
            }
        }

        if (vacio)
            return false;
    }

    return true;
}

void Solver::restaura(const int i, Dominio& factibles, Podados& podados)
{
    // Se ha dado un salto hacia atras
    fcBackJumps++;

    // Comprueba si la variable actual es responsable de la eliminacion del valor
    auto responsable = [](const int i, const std::pair<int, int> b){return i == b.first;};

    int N = podados.size();
    for (int j = i+1; j < N; ++j) {
        for (auto it = podados[j].begin(); it != podados[j].end();){
            if (responsable(i, *it)) {
                factibles.getDominio(j).insert(it->second);
                it = podados[j].erase(it);
            }
            else
                ++it;
        }
    }
}

bool Solver::cumpleRestriccion(const int a, const int b, const int i, const int j)
{
    if (!tab_rest)
        return false;

    auto it = tab_rest->find(std::make_pair(i, j));

    // Si no hay restriccion, se cumple
    if (it == tab_rest->end())
            return true;

    Restriccion r = it->second;

    switch (r) {
        case Restriccion::DIFERENTE:
            if (a != b) return true;
            else return false;
            break;
        case Restriccion::MAYOR_QUE:
            if (a < b) return true;
            else return false;
            break;
        case Restriccion::MENOR_QUE:
            if (a > b) return true;
            else return false;
            break;
        default:
            return false;
    }
}

void Solver::iniTableroRestriccion()
{
    if (!tablero)
        return;

    if (tab_rest)
        tab_rest.release();
    
    tab_rest = std::unique_ptr<TableroRest>(new TableroRest);

    // Consigue el indice de la variable
    auto variable = [this](const int i, const int j){return i*tablero->getSize() + j;};

    for (int i = 0; i < tablero->getSize(); ++i) {
        for (int j = 0; j < tablero->getSize(); ++j) {
            // Insertar las restricciones de valores diferentes para toda su columna y fila
            for (int k = 0; k < tablero->getSize(); ++k) {
                // Restricciones de diferencia por columnas
                if (j != k) {
                    tab_rest->insert({{variable(i, j), variable(i, k)}, Restriccion::DIFERENTE});
                }
                if (i != k) {
                    tab_rest->insert({ { variable(i, j), variable(k, j) }, Restriccion::DIFERENTE });
                }
            }
        }
    }

    for (int i = 0; i < tablero->getSize(); ++i) {
        for (int j = 0; j < tablero->getSize(); ++j) {
            // Insertar las restricciones de valores diferentes para toda su columna y fila
            for (int k = 0; k < tablero->getSize(); ++k) {
                // Insertar restricciones de mayor o menor que, en caso de que existan
                if (i < tablero->getSize()-1 && tablero->getElement(i*2+1, j*2)) {
                    tab_rest->erase({variable(i,   j), variable(i+1, j)});
                    tab_rest->erase({variable(i+1,   j), variable(i, j)});
                    if (tablero->getElement(i*2+1, j*2) == 1) {
                        tab_rest->insert({{variable(i,   j), variable(i+1, j)}, Restriccion::MAYOR_QUE });
                        tab_rest->insert({{variable(i+1, j), variable(i,   j)}, Restriccion::MENOR_QUE });
                    } else {
                        tab_rest->insert({{variable(i,   j), variable(i+1, j)}, Restriccion::MENOR_QUE });
                        tab_rest->insert({{variable(i+1, j), variable(i,   j)}, Restriccion::MAYOR_QUE });
                    }
                }
                if (j < tablero->getSize()-1 && tablero->getElement(i*2, j*2+1)) {
                    tab_rest->erase({variable(i, j), variable(i, j+1)});
                    tab_rest->erase({variable(i, j+1), variable(i, j)});
                    if (tablero->getElement(i*2, j*2+1) == 1) {
                        tab_rest->insert({{variable(i, j), variable(i, j+1)}, Restriccion::MAYOR_QUE });
                        tab_rest->insert({{variable(i, j+1), variable(i, j)}, Restriccion::MENOR_QUE });
                    } else {
                        tab_rest->insert({{variable(i, j), variable(i, j+1)}, Restriccion::MENOR_QUE });
                        tab_rest->insert({{variable(i, j+1), variable(i, j)}, Restriccion::MAYOR_QUE });
                    }
                }
            }
        }
    }
}

void Solver::guardarEnTablero(const FC_X& X)
{
    auto casilla = [this](const int V){return std::pair<int, int>(V / tablero->getSize(),
                                                                  V % tablero->getSize());};
    int i, j;
    for (size_t x = 0; x < X.size(); ++x) {
        std::tie(i, j) = casilla(x);
        tablero->setCasilla(i, j, X[x]);
    }
}
