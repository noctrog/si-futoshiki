#ifndef SOLVER_H
#define SOLVER_H

#include "tablero.h"

#include <utility>
#include <unordered_set>
#include <tuple>
#include <functional>
#include <chrono>
#include <memory>
#include <set>
#include <QObject>

// Clase utilizada para cronometrar cuanto tarda cada algoritmo
class Cronometro {
    public:
        Cronometro(const std::string nombre_algoritmo);
        virtual ~Cronometro();

    private:
        std::string nombre_algoritmo;
        std::chrono::steady_clock::time_point comienzo;
};

// Estructura de datos del dominio de todas las variables del problema
using dominio_tad = std::vector<std::set<int>>;

// Clase que facilita la interaccion con los dominios
class Dominio
{
    public:
        Dominio(const int size);
        Dominio(Tablero* const tablero);
        Dominio(const Dominio& rhs);
        ~Dominio();

        std::set<int>& getDominio(const int, const int);
        const std::set<int>& getDominio(const int, const int) const;
        std::set<int>& getDominio(const int);
        const std::set<int>& getDominio(const int) const;

        int getNumValores() const;
    private:
        const int size;
        dominio_tad dominio;
};

// using par_variables = std::pair<int, int>;
enum class Restriccion : uint8_t {
    MAYOR_QUE = 0xF,
    MENOR_QUE,
    DIFERENTE
};

// Calcula la funcion hash para almacenar los arcos en un unordered_set
using arco_hash = struct {
    template <class T1, class T2, class T3>
        std::size_t operator() (const std::tuple<T1, T2, T3>& arco) const {
            std::size_t hash_1 = std::get<0>(arco);
            std::size_t hash_2 = std::get<1>(arco) << 10;
            std::size_t hash_3 = static_cast<uint8_t>(std::get<2>(arco)) << 20;

            return hash_1 | hash_2 | hash_3;
        }
};


// Representa un arco <Vi, Vj> y la restriccion entre las dos variables
using arco = std::tuple<int, int, Restriccion>;
// Cola (set) para guardar los arcos que el algoritmo AC3 tiene que considerar
using cola_ac3 = std::unordered_set<arco, arco_hash>;

// Estructura de datos para almacenar las restricciones iniciales y acelerar su búsqueda
class ArcosCSP {
    using arcos_iniciales = std::map<int, std::vector<std::tuple<int, Restriccion>>>;
    public:
    ArcosCSP(Tablero* const tablero);
    ~ArcosCSP();

    // Devuelve un vector con todos los arcos con Vi como elemento a la derecha de la restriccion
    std::vector<arco> getRestriccionesVecinas(const int i) const;
    // Crea una cola de arcos con todos los arcos iniciales
    cola_ac3 crearCola(void) const;
    private:
    // Inserta una restriccion
    void insertarRestriccion(const int var1, const int var2, const Restriccion r);
    arcos_iniciales arcos;
};

// Estructura de datos que guarda las soluciones en Forward Checking
using FC_X = std::vector<int>;
// Estructura de datos que guarda las restricciones para ser miradas en el metodo forward
// (como las restricciones < y > ya llevan implicita el diff, solo hace falta guardar una
// restriccion)
using TableroRest = std::map<std::pair<int, int>, Restriccion>;
// Valores de variables podados, guardando la variable responsable de la poda
// first - valor responsable
// second - valor podado
using ValPod = std::pair<int, int>;
// Lista de todos los valores podados
using Podados = std::vector<std::set<ValPod>>;

class Solver : public QObject
{
    Q_OBJECT
    public:
        explicit Solver(QObject *parent = nullptr);

        // Backtracking
        void ejecutarBT(Tablero *);
        // AC3
        void ejecutarAC3(Tablero *);
        // Forward Checking
        void ejecutarFC(Tablero *);

    signals:

    public slots:

    private:
            // Backtracking
            bool backtracking(int, int, const Dominio&);
            bool btComprobarCasilla(const int, const int) const;
            // AC3
            bool AC3(Dominio&);
            bool noConsistente(const int val, const std::set<int>&, const Restriccion) const;
            // Forward Checking
            bool FC(const int, FC_X&, Dominio&, Podados& podados);
            bool forward(const int, const int, Dominio&, Podados& podados);
            void restaura(const int, Dominio& dominio, Podados& podados);
            bool cumpleRestriccion(const int a, const int b, const int i, const int j);
            void iniTableroRestriccion();
            void guardarEnTablero(const FC_X& X);

            // Datos
            Tablero* tablero;    // Se guarda el tablero a resolver para evitar copias innecesarias del puntero
            int backTrackingJumps;
            int fcBackJumps;
            std::unique_ptr<Dominio> dominio_ptr;   // Dominios calculados por AC3
            std::unique_ptr<TableroRest> tab_rest;  // Mapa de restricciones para FC
};

#endif // SOLVER_H
